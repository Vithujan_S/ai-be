import os
from connections.S3 import s3,s3_1
from const.const import BUCKET_NAME, FILE_URL, IMG_FORMAT, FILE_NOT_EXIST, DELETED_IMG_BUCKET_NAME
from werkzeug.utils import secure_filename
import time
from datetime import date
import botocore
import glob
import numpy as np
import cv2
import collections
from base64 import encodebytes
import io
from PIL import Image
class S3Bucket:
    def __init__(self, work_id):

        self.work_id = work_id
        self.current_date = date.today()

    # Rename and store image
    def renameFile_upload(self, file, imageName, workOrderID):

        # filename = secure_filename(file.filename)
        current_time = str(time.time() * 1000)
        current_time = current_time.replace(".","")
        # filename = str(self.work_id) + '_' + current_time + IMG_FORMAT
        file.save(imageName)
        directory = str(self.current_date) + '/' + str(self.work_id)
        s3.Bucket(BUCKET_NAME).upload_file(Filename = imageName, Key = directory + '/' + imageName)
        path = FILE_URL  + directory + '/' + imageName
        
        if os.path.exists(imageName):
            os.remove(imageName)
        else:
            print(FILE_NOT_EXIST + str(imageName))
        
        return path,imageName
    
    # Delete files
    def deletefile(self,key):
        s3.Object(BUCKET_NAME, key).delete()

        return "Delete successful"
    

    # Resubmiting and deleting img
    def Img_available(self, url, img_name, file, imageName, workOrderID):
        url = url.replace(FILE_URL,"")
        '''my_bucket = s3.Bucket(BUCKET_NAME)
        obj = s3.Bucket(BUCKET_NAME).Object(url).get()
        data = obj['Body']
        print(data)'''
        directory = str(self.current_date) + '/' + str(self.work_id) + '/' + img_name

        # Checking whether object iss available in S3 bucket
        # Move the object into another bucket
        try:
            s3.Object(BUCKET_NAME, url).load()
        except botocore.exceptions.ClientError as e:
            if e.response['Error']['Code'] == "404":
                print('object does not exsit')
            else:
                print("internal error")
                raise
        else:
            print("object exsit")
            copy_source = {'Bucket': BUCKET_NAME, 'Key': url}
            s3.meta.client.copy(copy_source, DELETED_IMG_BUCKET_NAME , directory)

        # Delete the moved object from current S3 buckets
        s3.Object(BUCKET_NAME, directory).delete()
        # s3_1.delete_object(Bucket = DELETED_IMG_BUCKET_NAME, Key = directory)    
        # s3.Object(Bucket = BUCKET_NAME, Key = url).delete()

        # Store the new file in the bucket
        path,imageName = self.renameFile_upload(file, imageName, workOrderID)
        return path,imageName

    def get_response_image(self, image_path):
        image_path = ' '.join([str(elem) for elem in image_path])
        pil_img = Image.open(image_path, mode ='r') # reads the PIL image
        byte_arr = io.BytesIO()
        pil_img.save(byte_arr, format='jpeg') # convert the PIL image to byte array
        encoded_img = encodebytes(byte_arr.getvalue()).decode('ascii') # encode as base64
        return encoded_img

# , img_name, work_order, major_index
    def getimages(self, image_name, work_order_id, msisdn):

        li = []
        img = []
        url = []
        # prefix = Listing files from a folder ''', Prefix="2022-02-01/1/"'''
        # print(str(self.current_date))
        response = s3_1.list_objects_v2(Bucket = BUCKET_NAME, Prefix = "2022-02-08" + "/" + str(self.work_id)) # str(self.current_date)

        files = response.get("Contents")
        for file in files:
            li.append(file['Key'])
        # path_parent = os.path.dirname(os.getcwd())
        path1 = "work_order" + str(work_order_id) + "_" + str(msisdn)
        isFile = os.path.isfile(path1)
        if isFile == False:
            os.mkdir(path1)
        else:
            pass
        os.chdir(path1)
        
        for i in range(len(li)):
            name = li[i]
            name = name.replace("2022-02-08" + "/" + str(self.work_id) + "/", "") # str(self.current_date)
            if name == image_name:
                # url.append(s3_1.generate_presigned_url('get_object', Params = {'Bucket': BUCKET_NAME , 'Key': li[i]}, ExpiresIn = 3600))
                s3.Bucket(BUCKET_NAME).download_file(Key = li[i], Filename = image_name)
                # obj = s3.Bucket(BUCKET_NAME).Object(li[i]).get().get('Body').read()
                # object = s3.Bucket(BUCKET_NAME).Object(li[i])['Body'].read()
        #data_path = os.path.join(path1)
        os.chdir('../')
        # obj.np
        '''file_stream = obj['Body']
        im = Image.open(file_stream)
        imgArray = np.array(im)'''
        # print(imgArray.shape)
        # array = np.array(obj['Body'].read())
        # array = cv2.imread(BytesIO(obj['Body'].read()))
        # file_stream = object['Body'].read()
        # im = Image.open(io.BytesIO(object))
        # file_stream = io.BytesIO()
        '''object.download_fileobj(file_stream)
        img = cv2.imread(file_stream)'''
        # super_dict1 = collections.defaultdict(list)

        for file in glob.glob(path1 + "/*.jpg"):
            path = glob.glob(path1 + "/*.jpg")
            encoded_img = self.get_response_image(path)
            my_message = 'here is my message' # create your message as per your need
            res =  { 'Status' : 'Success', 'message': my_message , 'ImageBytes': encoded_img}

            '''super_dict1['image'].appendfile({"image_name":image_name,
                    "image_object" : cv2.imread(file)
                })
            with open(file,'rb') as f:
                    im_b64 = base64.b64encode(f.read())
            with open(file) as im:
                    my_img = {'image' : im}
                # img = open(file, 'rb')
            my_img = {'image' : img,
                        'url' : url,
                        'name': image_name}
            my_img = {'filename' : 'dtv.jpg',
                'image': im_b64}
            my_img = {'url': url}'''
            #imgd = str(img_encoded)
                # img.append(cv2.imread(file))
        '''my_img = {'url': url,
                  'name' : image_name}'''
        # os.remove(path1 + '/' + image_name)
        return res, path1