class CreateDB(Resource):
    def post(self):
        args = db_put_args.parse_args()
        db = DB_config.DB_config(args['name'])
        db_name = db.db_name
        return jsonify({"Message": "Database name " + db_name})

class CreateTable(Resource):
    def post(self):
        args = db_put_args.parse_args()
        db = DB_config.DB_config(args['name'])
        res = db.createTable(tb_name = 'img_tbl')
        return jsonify({"Message": res})


api.add_resource(CreateDB, "/createDB")
api.add_resource(CreateTable, "/createTable")