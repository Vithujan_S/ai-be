# init a base image (Alpine is small Linux distro)
FROM python:3.8-slim-buster
# define the present working directory
WORKDIR /docker-flask
# copy the contents into the working dir
ADD . /docker-flask
# run pip to install the dependencies of the flask app
RUN pip3 install -r requirements.txt
EXPOSE 5000
# define the command to start the container
CMD ["python3","application.py"]